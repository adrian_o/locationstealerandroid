package pl.locationstealer.providers;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;

import pl.locationstealer.base.AndroidBus;


@EBean(scope = Scope.Singleton)
public class BusProvider
{
	private AndroidBus eventBus;

	public synchronized AndroidBus getEventBus()
	{
		if (eventBus == null)
		{
			eventBus = new AndroidBus();
		}

		return eventBus;
	}
}