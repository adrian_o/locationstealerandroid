package pl.locationstealer.communication;

import java.util.List;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;

import pl.locationstealer.communication.postparams.LocationParams;
import pl.locationstealer.entities.LocationEntity;
import pl.locationstealer.responses.LocationResponse;
import pl.locationstealer.utils.Consts;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.converter.GsonConverter;
import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@EBean(scope=Scope.Singleton)
public class CommunicationService {
	
	@RootContext
	protected Context context;

	private Api api;

	protected CommunicationService(){

		//Retrofit error!java.io.EOFException
		System.setProperty("http.keepAlive", "false");

		Gson gson = new GsonBuilder()  
		.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)  
		.create();  

		RestAdapter restAdapter =  new RestAdapter.Builder().setLogLevel(LogLevel.FULL).setEndpoint(Consts.webserviceBaseUrl).setConverter(new GsonConverter(gson))
				.build();
		this.api = restAdapter.create(Api.class);
	}
	
	public void postLocations(List<LocationEntity> locations, Callback<LocationResponse> c){
		String udid = android.provider.Settings.System.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
		LocationParams params = new LocationParams(locations, udid);
		api.postLocations(params, c);
	}

}


