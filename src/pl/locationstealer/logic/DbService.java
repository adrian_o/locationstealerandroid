package pl.locationstealer.logic;

import java.sql.SQLException;
import java.util.List;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;

import com.j256.ormlite.dao.Dao;

import pl.locationstealer.data.DatabaseHelper;
import pl.locationstealer.entities.LocationEntity;
import android.content.Context;
import android.location.Location;

@EBean(scope=Scope.Singleton)
public class DbService {

	@RootContext
	protected Context context;

	private DatabaseHelper dbHelper;

	public void configureDatabase(){
		this.dbHelper = new DatabaseHelper(this.context);
	}

	@AfterInject
	protected void afterInject()
	{ 
		configureDatabase();
	}
	
	public void saveLocation(Location l) throws SQLException{
		dbHelper.getDaoClassic(LocationEntity.class).create( new LocationEntity(l));
	}
	
	public List<LocationEntity> getLocationEntities() throws SQLException{
		Dao<LocationEntity, Integer> dao =  this.dbHelper.getDaoClassic(LocationEntity.class);
		return dao.queryForAll();
	}
	
	public void removeLocationEntity(LocationEntity le) throws SQLException{
		
		Dao<LocationEntity, Integer> dao =  this.dbHelper.getDaoClassic(LocationEntity.class);
		dao.delete(le);
	}

	public void removeLocationEntities(List<Integer> id) throws SQLException {
		Dao<LocationEntity, Integer> dao =  this.dbHelper.getDaoClassic(LocationEntity.class);
		dao.deleteIds(id);
	}
	
	public LocationEntity getLastLocationEntity() throws SQLException{
		Dao<LocationEntity, Integer> dao =  this.dbHelper.getDaoClassic(LocationEntity.class);
		return dao.queryForFirst(dao.queryBuilder().orderBy("id", false).limit(1L).prepare());
	}
	
}
