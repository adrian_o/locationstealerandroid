package pl.locationstealer.logic;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref()
interface SyncServicePrefs {
	
	String synchronizationDate();
	long synchronizationDateTimestamp();
}