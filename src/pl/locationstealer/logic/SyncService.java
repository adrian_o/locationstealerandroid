package pl.locationstealer.logic;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;

import pl.locationstealer.communication.CommunicationService;
import pl.locationstealer.entities.LocationEntity;
import pl.locationstealer.providers.BusProvider;
import pl.locationstealer.responses.LocationResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.content.Context;

@EBean(scope=Scope.Singleton)
public class SyncService {
	
	@RootContext
	protected Context context;
	
	@Bean
	protected DbService dbService;
	
	@Bean
	protected CommunicationService commService;
		
	@Pref
	SyncServicePrefs_ synchPrefs;
	
	@Bean
	protected BusProvider busProvider;
	
	public long getLastSynchronizationTimestamp(){
		long lastTimestamp = synchPrefs.synchronizationDateTimestamp().get();
		if( lastTimestamp > 0 ){
			return lastTimestamp;
		}else{
			Calendar c = Calendar.getInstance();
			c.setTimeZone(TimeZone.getTimeZone("UTC"));
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			return c.getTimeInMillis();
		}
	}
	
	public int synchronize(){
		int changes = 0;

		try {
			List<LocationEntity> locations = dbService.getLocationEntities();
			commService.postLocations(locations, new LocationPostCallback());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return changes;
	}
	
	private class LocationPostCallback implements Callback<LocationResponse>{
		
		@Override
		public void failure(RetrofitError arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void success(LocationResponse response, Response arg1) {
			try {
				dbService.removeLocationEntities(response.id);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
