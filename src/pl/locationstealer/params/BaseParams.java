package pl.locationstealer.params;

import java.util.Locale;


public class BaseParams {
	
	private String language;
	
	BaseParams(){
		language = Locale.getDefault().getDisplayName();
	}

	public String getLanguage() {
		return language;
	}
	
	public static String getLanguageStaticly() {
		return Locale.getDefault().getDisplayName();
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
