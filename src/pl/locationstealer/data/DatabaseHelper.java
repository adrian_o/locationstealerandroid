package pl.locationstealer.data;

import java.sql.SQLException;
import java.util.Hashtable;

import pl.locationstealer.R;
import pl.locationstealer.base.BaseEntity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	// any time you make changes to your database objects, you may have to increase the database version
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "elner.db";

	
	private Hashtable<Class<?>, Dao<? extends BaseEntity, Integer>> daos;
	private Hashtable<Class<?>, RuntimeExceptionDao<? extends BaseEntity, Integer>> daosRuntimeException;
	
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION,R.raw.ormlite_config);
		this.daos = new Hashtable<Class<?>, Dao<? extends BaseEntity,Integer>>();
		this.daosRuntimeException = new Hashtable<Class<?>, RuntimeExceptionDao<? extends BaseEntity,Integer>>();
	}
	
	/**
	 * This is called when the database is first created. Usually you should call createTable statements here to create
	 * the tables that will store your data.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		
		try {
			Log.i(DatabaseHelper.class.getName(), "onCreate");
			for (Class<?> clazz : DatabaseConfigUtil.classes) {
				TableUtils.createTableIfNotExists(connectionSource, clazz);
			}
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}
		
	}

	/**
	 * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
	 * the various data to match the new version number.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		
		try {
			Log.i(DatabaseHelper.class.getName(), "onUpgrade");
			for (Class<?> clazz : DatabaseConfigUtil.classes) {
				TableUtils.dropTable(connectionSource, clazz, true);
			}
			// after we drop the old databases, we create the new ones
			onCreate(db, connectionSource);
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void close() {
		super.close();

		this.daosRuntimeException.clear();
		this.daos.clear();
				
	}

	
	@SuppressWarnings("unchecked")
	public <S extends BaseEntity> Dao<S, Integer> getDaoClassic(Class<S> classModel) throws SQLException
	{
		if(!this.daos.containsKey(classModel))
		{
			Dao<S, Integer> d = getDao(classModel);
			this.daos.put(classModel, d);
		}
		return (Dao<S, Integer>) this.daos.get(classModel);
	}
	
	@SuppressWarnings("unchecked")
	public <S extends BaseEntity> RuntimeExceptionDao<S, Integer> getDaoRuntimeException(Class<S> classModel) throws SQLException
	{
		if(!this.daosRuntimeException.containsKey(classModel))
		{
			RuntimeExceptionDao<S, Integer> d = getRuntimeExceptionDao(classModel);
			this.daosRuntimeException.put(classModel, d);
		}
		return (RuntimeExceptionDao<S, Integer>) this.daosRuntimeException.get(classModel);
	}
	
	
	
}