package pl.locationstealer.data;

import pl.locationstealer.entities.LocationEntity;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {
	static final Class<?>[] classes = new Class[] {
		LocationEntity.class
	};
	public static void main(String[] args) throws Exception {
        writeConfigFile("ormlite_config.txt", classes);
	}
}
