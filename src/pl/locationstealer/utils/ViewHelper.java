package pl.locationstealer.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;

public class ViewHelper {

	public static int convertDpToPixels(Context c, int dp){
		final float scale = c.getResources().getDisplayMetrics().density;
		int pixels = (int) (dp * scale + 0.5f);
		return pixels;
	}
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static void setBackground(Context c, View v, int resId){
		if (android.os.Build.VERSION.SDK_INT >= 16)
		{
		    v.setBackground(c.getResources().getDrawable(resId));
		}
		else
		{
		    v.setBackgroundDrawable(c.getResources().getDrawable(resId));
		}
	}
}
