package pl.locationstealer.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DateHelper {

	public static Date parseDate(String dateString) throws ParseException{
		SimpleDateFormat parserSDF = new SimpleDateFormat(Consts.dateFormat, Locale.getDefault());
		Date date  = parserSDF.parse(dateString);
		return date;
	}

	public static String dateString(Date date){
		SimpleDateFormat parserSDF = new SimpleDateFormat(Consts.dateFormat, Locale.getDefault());
		String dateString = parserSDF.format(date);
		return dateString;
	}

	public static String dateTimeString(Date date){
		SimpleDateFormat parserSDF = new SimpleDateFormat(Consts.dateTimeFormat, Locale.getDefault());
		String dateString = parserSDF.format(date);
		return dateString;
	}

	public static String dateWithDayString(Date date){
		SimpleDateFormat parserSDF = new SimpleDateFormat(Consts.dateWithDayFormatString, Locale.getDefault());
		String dateString = parserSDF.format(date);
		return dateString;
	}	

	/**
	 * @param date1  the first date, not altered, not null
	 * @param date2  the second date, not altered, not null
	 * @return true if they represent the same day
	 * @throws IllegalArgumentException if either date is <code>null</code>
	 */
	public static boolean isSameDay(Date date1, Date date2) {
		if (date1 == null || date2 == null) {
			throw new IllegalArgumentException("The dates must not be null");
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		return isSameDay(cal1, cal2);
	}

	/**
	 * @param cal1  the first calendar, not altered, not null
	 * @param cal2  the second calendar, not altered, not null
	 * @return true if they represent the same day
	 * @throws IllegalArgumentException if either calendar is <code>null</code>
	 */
	public static boolean isSameDay(Calendar cal1, Calendar cal2) {
		if (cal1 == null || cal2 == null) {
			throw new IllegalArgumentException("The dates must not be null");
		}
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
				cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
				cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
	}

	/**
	 * <p>Checks if a date is today.</p>
	 * @param date the date, not altered, not null.
	 * @return true if the date is today.
	 * @throws IllegalArgumentException if the date is <code>null</code>
	 */
	public static boolean isToday(Date date) {
		return isSameDay(date, Calendar.getInstance().getTime());
	}

	/**
	 * @param cal  the calendar, not altered, not null
	 * @return true if cal date is today
	 * @throws IllegalArgumentException if the calendar is <code>null</code>
	 */
	public static boolean isToday(Calendar cal) {
		return isSameDay(cal, Calendar.getInstance());
	}

	public static Date getTodayDate(){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static String getTodaysDayName(int day){
		{  
			Map<Integer,String> mp = new HashMap<Integer,String>();  

			mp.put(1, "sunday");  
			mp.put(2, "monday");  
			mp.put(3, "tuesday");  
			mp.put(4, "wednesday");  
			mp.put(5, "thrusday");  
			mp.put(6, "friday");  
			mp.put(7, "saturday");  

			return mp.get(day);
		}  
	}
}
