package pl.locationstealer.utils;

public class Consts {
	public static final String dateFormat = "yyyy-MM-dd";
	public static final String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
	public static final String dateWithDayFormatString = "EEEE, yyyy-MM-dd";
	public static final String dateTimeWithDayFormatString = "EEEE, yyyy-MM-dd HH:mm:ss"; 
	public static final String webserviceBaseUrl = "http://student.agh.edu.pl/~aopyrch/path/web/app_dev.php";
}
