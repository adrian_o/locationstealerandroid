package pl.locationstealer.base;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

public class AndroidBus extends Bus {
	
	private final Handler mainThread = new Handler(Looper.getMainLooper());

	public void postOnMain(final Object event) {
		mainThread.post(new Runnable() {
			@Override
			public void run() {
				post(event);
			}
		});
	}
}