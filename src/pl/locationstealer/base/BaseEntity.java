package pl.locationstealer.base;

import com.j256.ormlite.field.DatabaseField;


public abstract class BaseEntity 
{
	@DatabaseField(generatedId = true, canBeNull=false)
	protected int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}
