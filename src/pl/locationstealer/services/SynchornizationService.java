package pl.locationstealer.services;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import pl.locationstealer.communication.CommunicationService;
import pl.locationstealer.logic.DbService;
import pl.locationstealer.logic.SyncService;
import pl.locationstealer.providers.BusProvider;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

@EService
public class SynchornizationService extends Service {

	@Bean
	protected BusProvider busProvider;
	
	@Bean
	protected CommunicationService commService;
	
	@Bean
	protected SyncService syncService;

	@Bean
	protected DbService dbService;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Toast.makeText(getApplicationContext(), "Synchronizuje",          
				Toast.LENGTH_LONG).show();
		Log.v("service", "ON!");
		syncService.synchronize();
		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
}