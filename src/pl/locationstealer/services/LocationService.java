package pl.locationstealer.services;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import pl.locationstealer.communication.CommunicationService;
import pl.locationstealer.logic.DbService;
import pl.locationstealer.logic.LocationHelper;
import pl.locationstealer.logic.SyncService;
import pl.locationstealer.providers.BusProvider;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

@EService
public class LocationService extends Service {

	@Bean
	protected BusProvider busProvider;
	
	@Bean
	protected CommunicationService commService;

	@Bean
	protected SyncService syncService;

	@Bean
	protected DbService dbService;
	
	@Bean
	protected LocationHelper locationHelper;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Toast.makeText(getApplicationContext(), "Lokalizacje!",          
				Toast.LENGTH_LONG).show();
		Log.v("service", "ON!");
		
		locationHelper.requestLocation();
		
		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	
}